# top
root -l -b -q 'ndimpdf.C++( 0, 0, 1, true)'
root -l -b -q 'ndimpdf.C++( 0, 0,-1, true)'
root -l -b -q 'ndimpdf.C++( 1, 0, 0, true)'
root -l -b -q 'ndimpdf.C++(-1, 0, 0, true)'
root -l -b -q 'ndimpdf.C++( 0, 1, 0, true)'
root -l -b -q 'ndimpdf.C++( 0,-1, 0, true)'
root -l -b -q 'ndimpdf.C++( 0, 0, 0.9, true)' # SM

# antitop
root -l -b -q 'ndimpdf.C++( 0, 0, 1, false)'
root -l -b -q 'ndimpdf.C++( 0, 0,-1, false)'
root -l -b -q 'ndimpdf.C++( 1, 0, 0, false)'
root -l -b -q 'ndimpdf.C++(-1, 0, 0, false)'
root -l -b -q 'ndimpdf.C++( 0, 1, 0, false)'
root -l -b -q 'ndimpdf.C++( 0,-1, 0, false)'
root -l -b -q 'ndimpdf.C++(-0.14, 0, 0.86, false)' # SM

