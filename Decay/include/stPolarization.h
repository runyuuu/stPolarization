//stPolarization.h
#ifndef stPolarization_H
#define stPolarization_H

#include "TLorentzVector.h"
#include "TVector3.h"
#include <cmath>
#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <cassert>
#include <random>
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TApplication.h"
#include "TROOT.h"
//#include "AtlasStyle.C"
#include <thread>
#include <ctime>
#include "TFile.h"
#include "TProfile.h"
#include "TTree.h"

class Generator {
public:
  Generator(unsigned int Nevents, double q, double mt, double mW, int topID, std::string pol);
    ~Generator();
    void select();
    void plot();
    void generate();
    void compute();
    void compute_weight(float& costheta, float& phi, float& costhetaS, float& phiS, float& weight);
    double outCosTheta();
    double outPhi();
    double outCosThetaStar();
    double outPhiStar();
    double cosTheta;
    double phi;
    double cosThetaStar;
    double phiStar;
    std::vector<double> harvestCosTheta();
    std::vector<double> harvestPhi();
    std::vector<double> harvestCosThetaStar();
    std::vector<double> harvestPhiStar();
    
private:
    std::random_device device;
    std::mt19937 e;
    const double MAXTHROW;
    unsigned int pickNumber;
    std::uniform_real_distribution<double> flatTheta;
    std::uniform_real_distribution<double> flatPhi;
    std::uniform_real_distribution<double> flatY;
    std::uniform_real_distribution<double> flatOne;
    std::vector<double> _cosTheta;
    std::vector<double> _phi;
    std::vector<double> _cosThetaStar;
    std::vector<double> _phiStar;
    size_t NPROC;
    std::vector<std::thread> tVector;
    unsigned int _Nevents;
    double _q, _mt, _mW;
    int _topID;
    std::string _pol;
};

#endif
