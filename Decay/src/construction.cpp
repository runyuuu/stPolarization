#include "protosDef.h"

TLorentzVector getNeutrino(singleTop sT){
    TLorentzVector neutrino;
    neutrino.SetPxPyPzE(sT.bblvq[0].pX,
                        sT.bblvq[0].pY,
                        sT.bblvq[0].pZ,
                        sqrt(pow(sT.bblvq[0].pX,2)
                             + pow(sT.bblvq[0].pY,2)
                             + pow(sT.bblvq[0].pZ,2)));
    return neutrino;
}

TLorentzVector getLepton(singleTop sT){
    TLorentzVector lepton;
    lepton.SetPxPyPzE(sT.bblvq[1].pX,
                      sT.bblvq[1].pY,
                      sT.bblvq[1].pZ,
                      sqrt(pow(sT.bblvq[1].pX,2)
                           +pow(sT.bblvq[1].pY,2)
                           +pow(sT.bblvq[1].pZ,2)
                           +pow(particleMass(sT.bblvq[1].particleId),2)));
    return lepton;
}

TLorentzVector getBottom(singleTop sT){
    TLorentzVector bottom;
    bottom.SetPxPyPzE(sT.bblvq[2].pX,
                      sT.bblvq[2].pY,
                      sT.bblvq[2].pZ,
                      sqrt(pow(sT.bblvq[2].pX,2)
                           +pow(sT.bblvq[2].pY,2)
                           +pow(sT.bblvq[2].pZ,2)
                           +pow(particleMass(sT.bblvq[2].particleId),2)));
    return bottom;
}

TLorentzVector getAntiBottom(singleTop sT){
    TLorentzVector antiBottom;
    antiBottom.SetPxPyPzE(sT.bblvq[3].pX,
                      sT.bblvq[3].pY,
                      sT.bblvq[3].pZ,
                      sqrt(pow(sT.bblvq[3].pX,2)
                           +pow(sT.bblvq[3].pY,2)
                           +pow(sT.bblvq[3].pZ,2)
                           +pow(particleMass(sT.bblvq[3].particleId),2)));
    return antiBottom;
}

TLorentzVector getQuarkSpec(singleTop sT){
    TLorentzVector quarkSpec;
    quarkSpec.SetPxPyPzE(sT.bblvq[4].pX,
                         sT.bblvq[4].pY,
                         sT.bblvq[4].pZ,
                         sqrt(pow(sT.bblvq[4].pX,2)
                              +pow(sT.bblvq[4].pY,2)
                              +pow(sT.bblvq[4].pZ,2)
                              +pow(particleMass(sT.bblvq[4].particleId),2)));
    return quarkSpec;
}

TLorentzVector getBeam(singleTop sT){
  TLorentzVector temp, Beam;
  temp = getQuarkSpec(sT);
  temp.SetPx(0.0);
  temp.SetPy(0.0);
  Beam.SetVectM(temp.Vect(),0.);
  return Beam;
}

TLorentzVector getGluon(singleTop sT){
    TLorentzVector gluon;

    for(int i=0; i<(sizeof(sT.qg)/sizeof(sT.qg[0])); ++i) 
      if(fabs(sT.qg[i].particleId)==21)
	gluon.SetPxPyPzE(0.,
                      0.,
                      sT.qg[i].pZ,
                      sqrt(pow(sT.qg[i].pZ,2)));
    return gluon;
}

TLorentzVector getInitquark(singleTop sT){
    TLorentzVector initquark;

    for(int i=0; i<(sizeof(sT.qg)/sizeof(sT.qg[0])); ++i) 
      if(fabs(sT.qg[i].particleId)!=21)
	initquark.SetPxPyPzE(0.,
                      0.,
                      sT.qg[i].pZ,
                      sqrt(pow(sT.qg[i].pZ,2)
                           +pow(particleMass(sT.qg[i].particleId),2)));
    return initquark;
}


TVector3 getBooster(TVector3 p, TVector3 pstar, double gamma) {
  float u2  = p.Mag2();
  float us2 = pstar.Mag2();
  float uus = p.Dot(pstar);
  float square = sqrt(fabs(1-4*u2+2*uus*(-1+gamma)+gamma+pow(uus,2)*(1+gamma)));
  // p.Print();
  // pstar.Print();
  // std::cout<<u2<<std::endl;
  // std::cout<<us2<<std::endl;
  // std::cout<<uus<<std::endl;
  // std::cout<<gamma<<std::endl;
  // std::cout<<square<<std::endl;
  
  float booster_x = (  p .X() * ( (1+uus)*(1+gamma) + sqrt(1+gamma)*square ) +
		    pstar.X() * ( (1-uus)*(1+gamma) + sqrt(1+gamma)*square - 2*u2*(1+gamma))  ) / (2*gamma*(u2+uus));
  float booster_y = (  p .Y() * ( (1+uus)*(1+gamma) + sqrt(1+gamma)*square ) + 
		    pstar.Y() * ( (1-uus)*(1+gamma) + sqrt(1+gamma)*square - 2*u2*(1+gamma))  ) / (2*gamma*(u2+uus));
  float booster_z = (  p .Z() * ( (1+uus)*(1+gamma) + sqrt(1+gamma)*square ) + 
  		    pstar.Z() * ( (1-uus)*(1+gamma) + sqrt(1+gamma)*square - 2*u2*(1+gamma))  ) / (2*gamma*(u2+uus));

  //int signz = (booster_z!=0)? booster_z/abs(booster_z) : 1.; // get the sign if the value is !=0
  //booster_z = signz*sqrt((1-1/pow(gamma,2)) - pow(booster_x,2) - pow(booster_y,2));
  
  TVector3 booster(booster_x,booster_y,booster_z);

  return booster;
}
