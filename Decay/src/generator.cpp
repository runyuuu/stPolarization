//This class generates 4 angles based on the 4 angle formula.
#include "pdf.cpp"
#include "stPolarization.h"
//Constructor
Generator::Generator(unsigned int Nevents, double q, double mt, double mW, int topID, std::string pol):cosTheta(0.0),phi(0.0),cosThetaStar(0.0),phiStar(0.0),e(device()),MAXTHROW(0.1),pickNumber(1),flatTheta(0.0,M_PI),flatPhi(0,2*M_PI),flatY(0.0,MAXTHROW),flatOne(0.0,1.0),NPROC(std::thread::hardware_concurrency()),_Nevents(Nevents),_q(q),_mt(mt),_mW(mW),_topID(topID), _pol(pol){
};
//Destructor
Generator::~Generator(){};

void Generator::compute(){
    Generator::generate();
    Generator::select();
    cosTheta = Generator::outCosTheta();
    phi = Generator::outPhi();
    cosThetaStar = Generator::outCosThetaStar();
    phiStar = Generator::outPhiStar();
};
void Generator::compute_weight(float& costheta, float& phi, float& costhetaS, float& phiS, float& weight){
    double q = _q;
    double mt = _mt;
    double mW = _mW;
    std::string polarization = _pol;
    double theta = flatTheta(e);
    costheta = cos(theta);
    phi = flatPhi(e);
    double thetaS = flatTheta(e);
    costhetaS = cos(thetaS);
    phiS = flatPhi(e);
    weight = PDF(theta, phi, thetaS, phiS,q,mt,mW,_topID,_pol);
};

void Generator::generate(){
    unsigned int N = _Nevents;
    double q = _q;
    double mt = _mt;
    double mW = _mW;
    std::string polarization = _pol;
    for(unsigned int i=0;i<N;i++){
        double thetaRan = flatTheta(e);
        double phiRan = flatPhi(e);
        double thetaSRan = flatTheta(e);
        double phiSRan = flatPhi(e);
        double y = flatY(e);
        if (y<PDF(thetaRan, phiRan, thetaSRan, phiSRan,q,mt,mW,_topID,_pol)){
            _cosTheta.push_back(cos(thetaRan));
            _phi.push_back(phiRan);
            _cosThetaStar.push_back(cos(thetaSRan));
            _phiStar.push_back(phiSRan);
        }
        else if (PDF(thetaRan, phiRan, thetaSRan, phiSRan,q,mt,mW,_topID,polarization)>MAXTHROW) {
	  throw std::runtime_error ("Function value exceeds max random number -> "+std::to_string(PDF(thetaRan, phiRan, thetaSRan, phiSRan,q,mt,mW,_topID,polarization)));
        }
        else if (PDF(thetaRan, phiRan, thetaSRan, phiSRan,q,mt,mW,_topID,polarization)<0) {
	  throw std::runtime_error ("The PDF function became less than 0! -> "+std::to_string(PDF(thetaRan, phiRan, thetaSRan, phiSRan,q,mt,mW,_topID,polarization)));
        }
    }
};
void Generator::select() {
    pickNumber = _cosTheta.size() * flatOne(e);
    if(pickNumber==0) pickNumber = 1;
};

double Generator::outCosTheta() {
    double value = _cosTheta[pickNumber-1];
    return value;
};

double Generator::outPhi() {
    double value = _phi[pickNumber-1];
    return value;
};

double Generator::outCosThetaStar() {
    double value = _cosThetaStar[pickNumber-1];
    return value;
};

double Generator::outPhiStar() {
    double value = _phiStar[pickNumber-1];
    return value;
};

