//The 4 angle formula from the fully differential top decay distribution paper (Eq13).
#include <cmath>
#include "TLorentzVector.h"
#include "TVector3.h"
#include "TMath.h"
#include <string>

double PDF(double theta, double phi, double thetaStar, double phiStar, double q, double mt, double mW, int topID, std::string pol){
    double px(0.0);
    double py(0.0);
    double pz(0.0);
    if (pol == "Z") {
        pz = 1.0;
    }else if (pol == "Z0") {
        pz = -1.0;
    }else if (pol == "X") {
        px = 1.0;
    }else if (pol == "X0") {
        px = -1.0;
    }else if (pol == "Y") {
        py = 1.0;
    }else if (pol == "Y0") {
        py = -1.0;
    }else if (pol == "STD") {
        if (topID>0) {
            pz = 0.88;
        }else{
            px = -0.11;
            pz = -0.85;
        }
    }else if (pol == "UNPHYS") {
        px = 1.0;
        py = 1.0;
        //pz = 1.0;
    }else {
        std::cout << "The polarization is not recognized." << std::endl;
        return 1;
    };
    TVector3 P(px,py,pz);
    TVector3 uL(sin(theta)*cos(phi),sin(theta)*sin(phi),cos(theta));
    TVector3 uT(cos(theta)*cos(phi),cos(theta)*sin(phi),-sin(theta));
    TVector3 uN(sin(phi),-cos(phi),0);
    double PuL = P.Dot(uL);
    double PuT = P.Dot(uT);
    double PuN = P.Dot(uN);
    double lambda(1);

    // mt = 172.5;
    // mW = 80.39;
    // q = sqrt(pow(mt*mt+mW*mW,2) - 4*mt*mt*mW*mW) / (2*mt);

    double a12   = (mt*mt-mW*mW-2*mt*q)/(mt*mt);
    double a0m2  = (mt*mt/2/mW/mW)*(mt*mt-mW*mW+2*mt*q)/(mt*mt);
    double a02   = (mt*mt/2/mW/mW)*(mt*mt-mW*mW-2*mt*q)/(mt*mt);
    double am1m2 = (mt*mt-mW*mW+2*mt*q)/(mt*mt);
    double a4 = (mt/mW/sqrt(2))*a12*cos(phiStar);
    double a5 = (mt/mW/sqrt(2))*am1m2*cos(phiStar);
    double a6 = (mt/mW/sqrt(2))*a12*(-sin(phiStar));
    double a7 = (mt/mW/sqrt(2))*am1m2*(-sin(phiStar));

    // // higher order terms in mb
    // double mb = 4.2;
    // a12  += (mb*mb)/(mt*mt);
    // a0m2 += (mb*mb/2/mW/mW)*(mb*mb-2*mt*mt-mW*mW-2*mt*q)/(mt*mt);
    // a02  += (mb*mb/2/mW/mW)*(mb*mb-2*mt*mt-mW*mW+2*mt*q)/(mt*mt);
    // am1m2+= (mb*mb)/(mt*mt);

    if(topID<0) {//+1 for tops and -1 for anti-tops.
      lambda = -1;
      std::swap(a12,am1m2);
      std::swap(a0m2,a02);
      std::swap(a4,a5);
      std::swap(a6,a7);
    }   

    double constant = 3./pow(8.*TMath::Pi(),2);
    double fancyN = a12+a02+a0m2+am1m2;
    
    double output = // abs( // nasty trick! 
                    constant/fancyN * sin(theta) * sin(thetaStar) *
                    ( (a12*pow(1+lambda*cos(thetaStar),2)+2*a0m2*sin(thetaStar)*sin(thetaStar)) * (1+PuL)
		      +(2*a02*sin(thetaStar)*sin(thetaStar)+am1m2*pow(1-lambda*cos(thetaStar),2))*(1-PuL)
		      +lambda*2*sqrt(2)*(a4*(1+lambda*cos(thetaStar))+a5*(1-lambda*cos(thetaStar)))*sin(thetaStar)*PuT
		      +lambda*2*sqrt(2)*(a6*(1+lambda*cos(thetaStar))+a7*(1-lambda*cos(thetaStar)))*sin(thetaStar)*PuN ) ; // );

//    double f1 = (a12+am1m2)/(a12 + a02 + a0m2 + am1m2);
//    double f1plus = a12/(a12+am1m2);
//    double f0plus = (a02)/(a02+a0m2);
//    std::cout << "f1: " << f1 << std::endl;
//    std::cout << "f1+: "<<f1plus << std::endl;
//    std::cout << "f0+: "<< f0plus << std::endl;
    
    return output;
}



