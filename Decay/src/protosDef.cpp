#include "protosDef.h"

singleTop std::istream& operator>> (std::istream& is, substrate &p){
    is >> p.particleId >> p.color >> p.flow >> p.pZ;
    return is;
}

singleTop std::istream& operator>> (std::istream& is, product &p)
{
    is >> p.particleId >> p.color >> p.flow >> p.pX >> p.pY >> p.pZ;
    return is;
}

singleTop std::istream& operator>> (std::istream& is, singleTop &st)
{
    is >> st.eventNumber >> st.weight >> st.eventQ;
    is >> st.qg[0];
    is >> st.qg[1];
    for (int i = 0; i < 5; i++) {
        is >> st.bblvq[i];
    }
    return is;
}

double singleTop::particleMass(int id){
    switch (id) {
        case 1:
        case -1:
            return 0.0048;
        case 2:
        case -2:
            return 0.0023;
        case 3:
        case -3:
            return 0.095;
        case 4:
        case -4:
            return 1.29;
        case 5:
        case -5:
            return 4.18;
        case 6:
        case -6:
            return 172.44;
        case 11:
        case -11:
            return 0.000511;
        case 13:
        case -13:
            return 0.10566;
        case 15:
        case -15:
            return 1.7768;
    }
    return 1;
}

TLorentzVector singleTop::getBeam(){//We choose quark to be the beam particle.
    TLorentzVector Beam;
    if (qg[0]!= 21 || qg[0]!= -21 ) {
        Beam.SetPxPyPzE(0,0,qg[0].pZ,sqrt(pow(particleMass(qg[0].particleId),2)
                                             + pow(qg[0].pZ,2)));
    }else{
        Beam.SetPxPyPzE(0,0,qg[1].pZ,sqrt(pow(particleMass(qg[1].particleId),2)
                                             + pow(qg[1].pZ,2)));
    }

    return Beam;
}

TLorentzVector singleTop::getNeutrino(){
    TLorentzVector neutrino;
    neutrino.SetPxPyPzE(bblvq[0].pX,
                        bblvq[0].pY,
                        bblvq[0].pZ,
                        sqrt(pow(bblvq[0].pX,2)
                             + pow(bblvq[0].pY,2)
                             + pow(bblvq[0].pZ,2)));
    return neutrino;
}

TLorentzVector singleTop::getLepton(){
    TLorentzVector lepton;
    lepton.SetPxPyPzE(bblvq[1].pX,
                      bblvq[1].pY,
                      bblvq[1].pZ,
                      sqrt(pow(bblvq[1].pX,2)
                           +pow(bblvq[1].pY,2)
                           +pow(bblvq[1].pZ,2)
                           +pow(particleMass(bblvq[1].particleId),2)));
    return lepton;
}

TLorentzVector singleTop::getBottom(){
    TLorentzVector bottom;
    bottom.SetPxPyPzE(bblvq[2].pX,
                      bblvq[2].pY,
                      bblvq[2].pZ,
                      sqrt(pow(bblvq[2].pX,2)
                           +pow(bblvq[2].pY,2)
                           +pow(bblvq[2].pZ,2)
                           +pow(particleMass(bblvq[2].particleId),2)));
    return bottom;
}

TLorentzVector singleTop::getAntiBottom(){
    TLorentzVector antiBottom;
    antiBottom.SetPxPyPzE(bblvq[3].pX,
                      bblvq[3].pY,
                      bblvq[3].pZ,
                      sqrt(pow(bblvq[3].pX,2)
                           +pow(bblvq[3].pY,2)
                           +pow(bblvq[3].pZ,2)
                           +pow(particleMass(bblvq[3].particleId),2)));
    return antiBottom;
}

TLorentzVector singleTop::getQuarkSpec(){
    TLorentzVector quarkSpec;
    quarkSpec.SetPxPyPzE(bblvq[4].pX,
                         bblvq[4].pY,
                         bblvq[4].pZ,
                         sqrt(pow(bblvq[4].pX,2)
                              +pow(bblvq[4].pY,2)
                              +pow(bblvq[4].pZ,2)));
    return quarkSpec;
}

