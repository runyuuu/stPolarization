//
//  main.cpp
//
//
//  Created by Runyu on 10/31/17.
//

#include "stPolarization.h"
#include "construction.cpp"
#include "getAngles.cpp"
#include "generator.cpp"
#include "protosDef.h"
#include <iomanip>

int main(int argc, char **argv)
{
  std::string usage = std::string("Usage: ")
    + argv[0]
    + " [-?] [-p polarization] [-g generation]\n"
    + "generation options: 0,1,2 stand for \"no generation\", \"decay only\", \"production and decay\""
    + "polarization options: Z,Z0,X,X0,Y,Y0,STD\n";
    
  if (argc>1 && argv[1]==std::string("-?")) {
    std::cout << usage << std::endl;
    exit (0);
  }
  std::string pol = "STD";
  int gen = 1;
  try {
    // overwritten by command line:
    for (int i=1; i<argc;i+=2) {
      std::istringstream stream(argv[i+1]);
      if (std::string(argv[i])=="-g") stream >> gen;
      if (std::string(argv[i])=="-p") stream >> pol;                                                

      if(gen==0) pol="OG";  
      //            if (pol!="+Z"||"-Z"||"+X"||"-X"||"+Y"||"-Y") {
      //                std::cout << "Polarization not recongized" << std::endl;
      //                exit(0);
      //            }
    }
  }
  catch (std::exception &) {
    std::cout << usage << std::endl;
    exit (0);
  }
    

  std::clock_t begin = clock();
  double Size(0);
  //SetAtlasStyle();
  gROOT->Reset();
  std::vector<singleTop> sT;
 
  //std::string filename = "../../Events/sample" + pol + ".wgt";
  //std::string filename = "../../Events/tbjLargeOld.unw";
  //std::string filename = "../../Events/group.phys-gener.protos22b.117788.st_tchan_lept_tbj_SM_CTEQ6L1_13TeV.TXT.mc15_v1._00001.events";
  //std::string filename = "../../../data/PROTOSfiles/tbjLarge.unw";
  //std::string filename = "../../../data/PROTOSfiles/tbj8TeV.unw";
  std::string filename = "../../Events/tbj5Feb.unw";

  std::ifstream inStream(filename);
  if (inStream.is_open()) {
    while (!inStream.eof()) {
      singleTop a;
      inStream >> a;
      sT.push_back(a);
    }
  }else{
    std::cout
      << "The event file is not found!\n"
      << "Quitting..."
      << std::endl;
    return 1;
  }
  std::ofstream ofs (Form("sTsample%s.events",pol.c_str()), std::ofstream::out);
    
  TFile hfile(Form("stPolarization%s.root",pol.c_str()),"RECREATE","Generated Decay products of Top.");
  typedef struct {Float_t cos_Theta,Phi_,cos_Theta_Star,Phi_Star;} FOURANGLES;
  static FOURANGLES fourangles;

  static Float_t _cosThetaX;
  static Float_t _cosThetaY;
  static Float_t _cosThetaZ;

  // typedef struct {Float_t cos_Theta_X,cos_Theta_Y,cos_Theta_Z;} XYZ;
  //static XYZ _xyz;
  typedef struct {Int_t eventNumber; Float_t weight, eventQ;} BOOKKEEPING;
  static BOOKKEEPING _book;
  typedef struct {Int_t id,color,flow; Float_t pt,eta,phi,mass;} PARTICLE;
  static PARTICLE _top;
  static PARTICLE _W;
  static PARTICLE _bottom;
  static PARTICLE _antibottom;
  static PARTICLE _spectatorQuark;
  static PARTICLE _lepton;
  static PARTICLE _neutrino;
  static PARTICLE _WT;
  static PARTICLE _leptonW;
  typedef struct {Int_t id,color,flow; Float_t pz,mass;} BEAMLINE;
  static BEAMLINE _beam1;
  static BEAMLINE _beam2;

  TTree *tree = new TTree("singleTop","Angles of such polarization.");
  tree->Branch("fourangles",&fourangles,"cos_Theta/F:Phi_/F:cos_Theta_Star/F:Phi_Star/F");
  //tree->Branch("xyz",&_xyz,"cos_Theta_X:cos_Theta_Y:cos_Theta_Z");

  tree->Branch("_cosThetaX",&_cosThetaX,"_cosThetaX/F");
  tree->Branch("_cosThetaY",&_cosThetaY,"_cosThetaY/F");
  tree->Branch("_cosThetaZ",&_cosThetaZ,"_cosThetaZ/F");
  tree->Branch("Event",&_book,"eventNumber/i:weight/F:eventQ/F");
  tree->Branch("_top",&_top,"id/I:color/I:flow/I:pt/F:eta/F:phi/F:mass/F");
  tree->Branch("_W",&_W,"id/I:color/I:flow/I:pt/F:eta/F:phi/F:mass/F");
  tree->Branch("_bottom",&_bottom,"id/I:color/I:flow/I:pt/F:eta/F:phi/F:mass/F");
  tree->Branch("_antibottom",&_bottom,"id/I:color/I:flow/I:pt/F:eta/F:phi/F:mass/F");
  tree->Branch("_spectatorQuark",&_spectatorQuark,"id/I:color/I:flow/I:pt/F:eta/F:phi/F:mass/F");
  tree->Branch("_beam1",&_beam1,"id/I:color/I:flow/I:pz/F:mass/F");
  tree->Branch("_beam2",&_beam2,"id/I:color/I:flow/I:pz/F:mass/F");
  tree->Branch("_lepton",&_lepton,"id/I:color/I:flow/I:pt/F:eta/F:phi/F:mass/F");
  tree->Branch("_neutrino",&_neutrino,"id/I:color/I:flow/I:pt/F:eta/F:phi/F:mass/F");
  tree->Branch("_WT",&_WT,"id/I:color/I:flow/I:pt/F:eta/F:phi/F:mass/F");
  tree->Branch("_leptonW",&_leptonW,"id/I:color/I:flow/I:pt/F:eta/F:phi/F:mass/F");

  TH1D* hCosTheta = new TH1D("hCosTheta", "CosTheta",50,-1,1);
  hCosTheta -> SetMarkerStyle(2);
  hCosTheta -> SetMinimum(0);
  TH1D* hPhi = new TH1D("hPhi", "Phi",50,0,2*M_PI);
  hPhi -> SetMarkerStyle(2);
  hPhi -> SetMinimum(0);
  TH1D* hCosThetaStar = new TH1D("hCosThetaStar", "CosThetaStar",50,-1,1);
  hCosThetaStar -> SetMarkerStyle(2);
  hCosThetaStar -> SetMinimum(0);
  TH1D* hPhiStar = new TH1D("hPhiStar", "PhiStar",50,0,2*M_PI);
  hPhiStar -> SetMarkerStyle(2);
  hPhiStar -> SetMinimum(0);
  TH1D* hCosThetaX = new TH1D("hCosThetaX", "CosThetaX",50,-1,1);
  hCosThetaX -> SetMarkerStyle(2);
  hCosThetaX -> SetMinimum(0);
  TH1D* hCosThetaY = new TH1D("hCosThetaY", "CosThetaY",50,-1,1);
  hCosThetaY -> SetMarkerStyle(2);
  hCosThetaY -> SetMinimum(0);
  TH1D* hCosThetaZ = new TH1D("hCosThetaZ", "CosThetaZ",50,-1,1);
  hCosThetaZ -> SetMarkerStyle(2);
  hCosThetaZ -> SetMinimum(0);
    
  TH1D* hCosThetaRe = new TH1D("hCosThetaRe", "CosTheta",50,-1,1);
  hCosThetaRe -> SetMarkerStyle(2);
  hCosThetaRe -> SetMinimum(0);
  TH1D* hPhiRe = new TH1D("hPhiRe", "Phi",50,0,2*M_PI);
  hPhiRe -> SetMarkerStyle(2);
  hPhiRe -> SetMinimum(0);
  TH1D* hCosThetaStarRe = new TH1D("hCosThetaStarRe", "CosThetaStar",50,-1,1);
  hCosThetaStarRe -> SetMarkerStyle(2);
  hCosThetaStarRe -> SetMinimum(0);
  TH1D* hPhiStarRe = new TH1D("hPhiStarRe", "PhiStar",50,0,2*M_PI);
  hPhiStarRe -> SetMarkerStyle(2);
  hPhiStarRe -> SetMinimum(0);
    
  for (int i = 0; i < sT.size()  ; i++) {
    //std::cout<<"---> event "<<i<<std::endl;

    TLorentzVector beam;
    TLorentzVector beamT;
    TLorentzVector beamW;
    TLorentzVector top;
    TLorentzVector W;
    TLorentzVector bottom;
    TLorentzVector bottomT;
    TLorentzVector bottomW;
    TLorentzVector antiBottom;
    TLorentzVector quarkSpec;
    TLorentzVector quarkSpecT;
    TLorentzVector quarkSpecW;
    TLorentzVector lepton;
    TLorentzVector leptonT;
    TLorentzVector leptonW;
    TLorentzVector neutrino;
    TLorentzVector neutrinoT;
    TLorentzVector neutrinoW;
    TLorentzVector buffer;
    TLorentzVector WT;
    Float_t cosTheta;
    Float_t cosThetaStar;
    Float_t phi;
    Float_t phiStar;
    Float_t cosThetaX;
    Float_t cosThetaY;
    Float_t cosThetaZ;
    Float_t pW;
    Float_t pL;
    Float_t pB;
    Float_t pN;
    TVector3 T; //X-axis in top
    TVector3 N; //-Y-axis
    TVector3 q; //Z-axis
    TLorentzVector XB;
    TLorentzVector YB;

    TVector3 Tprime; //X-axis in W
    TVector3 Nprime; //-Y-axis
    TVector3 qprime; //Z-axis
        
    _book.eventNumber = sT[i].eventNumber;
    _book.weight = sT[i].weight;
    _book.eventQ = sT[i].eventQ;
        
    beam = getBeam(sT[i]);
    _beam1.id = sT[i].qg[0].particleId;
    _beam1.color = sT[i].qg[0].color;
    _beam1.flow = sT[i].qg[0].flow;
    _beam1.pz = sT[i].qg[0].pZ;
    _beam1.mass = particleMass(sT[i].qg[0].particleId);
    // std::cout<<"beam: "<<std::endl;
    // beam.Print();        

    _beam2.id = sT[i].qg[1].particleId;
    _beam2.color = sT[i].qg[1].color;
    _beam2.flow = sT[i].qg[1].flow;
    _beam2.pz = sT[i].qg[1].pZ;
    _beam2.mass = particleMass(sT[i].qg[1].particleId);
        
    neutrino = getNeutrino(sT[i]);
    _neutrino.color = sT[i].bblvq[0].color;
    _neutrino.flow = sT[i].bblvq[0].flow;
    lepton = getLepton(sT[i]);
    _lepton.color = sT[i].bblvq[1].color;
    _lepton.flow = sT[i].bblvq[1].flow;
    bottom = getBottom(sT[i]);
    _bottom.color = sT[i].bblvq[2].color;
    _bottom.flow = sT[i].bblvq[2].flow;
    // std::cout<<"neutrino: "<<std::endl;
    // neutrino.Print();        
    // std::cout<<"lepton: "<<std::endl;
    // lepton.Print();        
    // std::cout<<"bottom: "<<std::endl;
    // bottom.Print();        
        
    antiBottom = getAntiBottom(sT[i]);
    _antibottom.id = sT[i].bblvq[3].particleId;
    _antibottom.color = sT[i].bblvq[3].color;
    _antibottom.flow = sT[i].bblvq[3].flow;
    _antibottom.pt = antiBottom.Pt();
    _antibottom.eta = antiBottom.Eta();
    _antibottom.phi = antiBottom.Phi();
    _antibottom.mass = antiBottom.M();
    // std::cout<<"antiBottom: "<<std::endl;
    // antiBottom.Print();        
        
    quarkSpec = getQuarkSpec(sT[i]);
    _spectatorQuark.id = sT[i].bblvq[4].particleId;
    _spectatorQuark.color = sT[i].bblvq[4].color;
    _spectatorQuark.flow = sT[i].bblvq[4].flow;
    _spectatorQuark.pt = quarkSpec.Pt();
    _spectatorQuark.eta = quarkSpec.Eta();
    _spectatorQuark.phi = quarkSpec.Phi();
    _spectatorQuark.mass = quarkSpec.M();
    // std::cout<<"quarkSpec: "<<std::endl;
    // quarkSpec.Print();        

    //Reconstruct top and W; Boost W into top and record its momentum:
    top = neutrino + bottom + lepton;
    _top.pt = top.Pt();
    _top.eta = top.Eta();
    _top.phi = top.Phi();
    _top.mass = top.M();
    if (sT[i].bblvq[0].particleId > 0) {
      _top.id = 6;
    }else{
      _top.id = -6;
    }
    // std::cout<<"top: "<<std::endl;
    // top.Print();        


    // FORCING POLARIZAITON IN PRODUCTION
    TLorentzVector bottominit, Wprod, WprodT;
    float pWprod, cosThetaprod, phiprod;
    //bottominit = getGluon(sT[i]) - antiBottom; // g -> anti-b + b
    Wprod = getInitquark(sT[i]) - quarkSpec; // q -> q_s + Wprod
    buffer = Wprod;
    buffer.Boost(-top.BoostVector());
    WprodT = buffer;
    pWprod = WprodT.P();

    buffer = quarkSpec;
    buffer.Boost(-top.BoostVector());
    quarkSpecT = buffer;
    buffer = beam;
    buffer.Boost(-top.BoostVector());
    beamT = buffer;
 
    //Construct q,N,T coord for PRODUCTION:
    q = quarkSpecT.Vect(); //Z-axis defined.
    q.SetMag(1);
    N = beamT.Vect().Cross(q);  //-Y-axis defined. N = st X q.
    N.SetMag(1);
    T = q.Cross(N); //X-axis defined. T = q X N.
    T.SetMag(1);
  
    // Generate 4 angles:
    if(gen>1) {
      Generator Geninit(10000,pWprod,top.M(),80.385,_top.id,pol); // using a 2-D pdf would speed up a lot!!!  consider W real when we need it in the generation!! quite hursh assumption!
      Geninit.compute();
      cosThetaprod = Geninit.cosTheta;
      phiprod = Geninit.phi;

      TVector3 tempprod = sin(acos(cosThetaprod))*cos(phiprod)*(T) + sin(acos(cosThetaprod))*sin(phiprod)*(-N) + cosThetaprod*(q);
      WprodT.SetVectM(pWprod*tempprod, Wprod.M());

      if(Wprod.M2()<=0.) { 
	Wprod .SetVectM(Wprod .Vect(),80.385); 
	WprodT.SetVectM(WprodT.Vect(),80.385); 
      }
      TVector3 vtop = getBooster(Wprod.BoostVector(), WprodT.BoostVector(), top.Gamma()); // beta1, beta2, gamma
      //vtop.Print();
      //top.Print();
      top.SetTheta(vtop.Theta()); top.SetPhi(vtop.Phi());
      //top.Print();
    }
    else {
      cosThetaprod = cos(WprodT.Angle(q));
      phiprod      = getPhi(top,Wprod,T,-N,q);
    }
      
    //Boost W, bottom spectator quark and beam into top;
    W = neutrino + lepton; // W = N + lepton ; only wants its abs momentum.
    buffer = W;
    buffer.Boost(-top.BoostVector());
    WT = buffer;
    pW = WT.P();
    buffer = bottom;
    buffer.Boost(-top.BoostVector());
    pB = buffer.P();
    bottomT = buffer;
        
    buffer = lepton;
    buffer.Boost(-top.BoostVector());
    leptonT = buffer;
    buffer = neutrino;
    buffer.Boost(-top.BoostVector());
    neutrinoT = buffer;
        
    buffer = quarkSpec;
    buffer.Boost(-top.BoostVector());
    quarkSpecT = buffer;
    buffer = beam;
    buffer.Boost(-top.BoostVector());
    beamT = buffer;
        
    //Construct q,N,T coord:
    q = quarkSpecT.Vect(); //Z-axis defined.
    q.SetMag(1);
    N = beamT.Vect().Cross(q);  //-Y-axis defined. N = st X q.
    N.SetMag(1);
    T = q.Cross(N); //X-axis defined. T = q X N.
    T.SetMag(1);
        
    XB.SetVectM(T, 0);
    YB.SetVectM(-N, 0);
        
    //Boost spectator, bottom, lepton and neutrino into W; record their momentums:
    buffer = quarkSpecT;
    buffer.Boost(-WT.BoostVector());
    quarkSpecW = buffer;
    buffer = beam;
    buffer.Boost(-WT.BoostVector());
    beamW = buffer;
                
    buffer = bottomT;
    buffer.Boost(-WT.BoostVector());
    bottomW = buffer;
        
    buffer = leptonT;
    buffer.Boost(-WT.BoostVector());
    leptonW = buffer;
    pL = leptonW.P();
        
    buffer = neutrinoT;
    buffer.Boost(-WT.BoostVector());
    pN = buffer.P();
	
    // Generate 4 angles:
    if(gen>0) {
      Generator Gen(10000,pW,top.M(),W.M(),_top.id,pol);
       Gen.compute();
       cosTheta = Gen.cosTheta;
       phi = Gen.phi;
       cosThetaStar = Gen.cosThetaStar;
       phiStar = Gen.phiStar;
//      Gen.compute_weight(cosTheta,phi,cosThetaStar,phiStar,_book.weight);
      fourangles.cos_Theta = cosTheta;
      fourangles.Phi_ = phi;
      fourangles.cos_Theta_Star = cosThetaStar;
      fourangles.Phi_Star=phiStar;
      //Plot the generated 4 angles.
      hCosTheta->Fill(cosTheta,_book.weight);
      hPhi->Fill(phi,_book.weight);
      hCosThetaStar->Fill(cosThetaStar,_book.weight);
      hPhiStar->Fill(phiStar,_book.weight);
    }
    else {
      cosTheta = cos(WT.Angle(q));
      phi      = getPhi(top,W,T,-N,q);
      fourangles.cos_Theta = cosTheta;
      fourangles.Phi_ = phi;
      //Plot 2 angles.
      hCosTheta->Fill(cosTheta,_book.weight);
      hPhi->Fill(phi,_book.weight);
    }
	

    //Apply the formula to recreate WT:
    TVector3 temp = sin(acos(cosTheta))*cos(phi)*(T) + sin(acos(cosTheta))*sin(phi)*(-N) + cosTheta*(q);
    //pW = sqrt(pow(top.M2()+WT.M2(),2) - 4*top.M2()*WT.M2()) / 2*top.M(); // value according to the energy conservation, rely on Generator for the moment
    WT.SetVectM(pW*temp, W.M());
    bottomT.SetVectM(-pB*temp, bottom.M());
    
    //Boost again spectator, bottom, lepton and neutrino into new W:
    buffer = quarkSpecT;
    buffer.Boost(-WT.BoostVector());
    quarkSpecW = buffer;
        
    buffer = bottomT;
    buffer.Boost(-WT.BoostVector());
    bottomW = buffer;
 
    //construct q',N',T' coord:
    qprime = -bottomW.Vect(); //Z-axis defined.
    qprime.SetMag(1);
    // XB.Boost(-WT.BoostVector());
    // YB.Boost(-WT.BoostVector());
    Nprime = -(XB.Vect().Cross(YB.Vect())).Cross(qprime);
    //Nprime = -beamT.Vect().Cross(qprime);
    //(problematic definition) Nprime = quarkSpecW.Vect().Cross(qprime);  //-Y-axis defined. N = st X q.
    Nprime.SetMag(1);
    Tprime = qprime.Cross(Nprime); //X-axis defined. T = q X N.
    Tprime.SetMag(1);

    if(gen<1) {
      cosThetaStar = cos(leptonW.Angle(qprime));
      phiStar      = getPhi(WT,leptonT,Tprime,-Nprime,qprime);
      fourangles.cos_Theta_Star = cosThetaStar;
      fourangles.Phi_Star=phiStar;
      //Plot the generated 4 angles.
      hCosThetaStar->Fill(cosThetaStar,_book.weight);
      hPhiStar->Fill(phiStar,_book.weight);	
    }

    //Apply the formula to recreate lepton and neutrino:
    temp = sin(acos(cosThetaStar))*cos(phiStar)*(Tprime) + sin(acos(cosThetaStar))*sin(phiStar)*(-Nprime) + cosThetaStar*(qprime);
    //pL = sqrt(pow(WT.M2()+leptonW.M2(),2) - 4*WT.M2()*leptonT.M2()) / 2*WT.M(); // value according to the energy conservation, rely on Generator for the moment
    leptonW.SetVectM(pL*temp, lepton.M());
    neutrinoW.SetVectM(-pN*temp, neutrino.M());
    
    //Boost everything back into lab frame. Record everything:
    buffer = WT;
    buffer.Boost(top.BoostVector());
    W = buffer;
    _W.pt = W.Pt();
    _W.eta = W.Eta();
    _W.phi = W.Phi();
    _W.mass = W.M();
    if (sT[i].bblvq[0].particleId > 0) {
      _W.id = 24;
    }else{
      _W.id = -24;
    }
    buffer = bottomT;
    buffer.Boost(top.BoostVector());
    bottom = buffer;
    _bottom.pt = bottom.Pt();
    _bottom.eta = bottom.Eta();
    _bottom.phi = bottom.Phi();
    _bottom.mass = bottom.M();
    _bottom.id = sT[i].bblvq[2].particleId;
    _bottom.color = sT[i].bblvq[2].color;
    _bottom.flow = sT[i].bblvq[2].flow;
        
    //Boost them back into lab frame. Record everything. (Going back to the top frame -> lab frame)
    buffer = leptonW;
    buffer.Boost(WT.BoostVector());
    leptonT = buffer;
    buffer.Boost(top.BoostVector());
    lepton = buffer;
        
    buffer = neutrinoW;
    buffer.Boost(WT.BoostVector());
    neutrinoT = buffer;
    buffer.Boost(top.BoostVector());
    neutrino = buffer;
 
    _lepton.id = sT[i].bblvq[1].particleId;
    _lepton.color = sT[i].bblvq[1].color;
    _lepton.flow = sT[i].bblvq[1].flow;
    _lepton.pt = lepton.Pt();
    _lepton.eta = lepton.Eta();
    _lepton.phi = lepton.Phi();
    _lepton.mass = lepton.M();
        
    _neutrino.id = sT[i].bblvq[0].particleId;
    _neutrino.color = sT[i].bblvq[0].color;
    _neutrino.flow = sT[i].bblvq[0].flow;
    _neutrino.pt = neutrino.Pt();
    _neutrino.eta= neutrino.Eta();
    _neutrino.phi = neutrino.Phi();
    _neutrino.mass = neutrino.M();

    _WT.pt = WT.Pt();
    _WT.eta = WT.Eta();
    _WT.phi = WT.Phi();
    _WT.mass = WT.M();
     if (sT[i].bblvq[0].particleId > 0) {
      _WT.id = 24;
    }else{
      _WT.id = -24;
    }
    _leptonW.id = sT[i].bblvq[1].particleId;
    _leptonW.color = sT[i].bblvq[1].color;
    _leptonW.flow = sT[i].bblvq[1].flow;
    _leptonW.pt = leptonW.Pt();
    _leptonW.eta = leptonW.Eta();
    _leptonW.phi = leptonW.Phi();
    _leptonW.mass = leptonW.M();
 
    //Get the thetaXYZ angles:
    cosThetaX = cos(leptonT.Angle(T));
    cosThetaY = cos(leptonT.Angle(-N));
    cosThetaZ = cos(leptonT.Angle(q));
    _cosThetaX = cosThetaX;
    _cosThetaY = cosThetaY;
    _cosThetaZ = cosThetaZ;
    hCosThetaX->Fill(cosThetaX,_book.weight);
    hCosThetaY->Fill(cosThetaY,_book.weight);
    hCosThetaZ->Fill(cosThetaZ,_book.weight);
        
        
    //Calculating angles again:
    cosTheta = cos(WT.Angle(q));
    phi = getPhi(top,W,T,-N,q);// phi = angle between x axis and W's projection on x-y plane.
    cosThetaStar = cos(leptonW.Angle(qprime));
    phiStar = getPhi(WT,leptonT,Tprime,-Nprime,qprime);// phi* = angle between x axis and l's projection on x-y plane (W frame).
    hCosThetaRe->Fill(cosTheta,_book.weight);
    hPhiRe->Fill(phi,_book.weight);
    hCosThetaStarRe->Fill(cosThetaStar,_book.weight);
    hPhiStarRe->Fill(phiStar,_book.weight);      
    //Write into Protos-like txt file.
    ofs << "\t" << sT[i].eventNumber << " " << std::setprecision(8) << std::scientific << _book.weight << "  " <<  sT[i].eventQ << std::endl;
    ofs << std::setw(3) << std::right << sT[i].qg[0].particleId << "  " << std::right << sT[i].qg[0].color << "  " << std::right << sT[i].qg[0].flow << "  "  << std::right << std::setprecision(8) << std::scientific << sT[i].qg[0].pZ << std::endl;
    ofs << std::setw(3) << sT[i].qg[1].particleId << "  " << sT[i].qg[1].color << "  " << sT[i].qg[1].flow << "  " << std::setprecision(8) << std::scientific << sT[i].qg[1].pZ << std::endl;

    ofs << std::setw(3) << _neutrino.id << "  " << _neutrino.color << "  " << _neutrino.flow << "  " << std::right << std::setprecision(8) << neutrino.Px() << "  " << std::right << std::right << std::setprecision(8) << neutrino.Py() << "  " << std::right <<  std::right << std::setprecision(8) << neutrino.Pz() << std::endl;
    ofs << std::setw(3) << _lepton.id << "  " <<_lepton.color << "  " << _lepton.flow << "  " << lepton.Px() << "  " << lepton.Py() << "  " << lepton.Pz() << std::endl;
    ofs << std::setw(3) << _bottom.id << "  " << _bottom.color << "  " << _bottom.flow << "  " << bottom.Px() << "  " << bottom.Py() << "  " << bottom.Pz() << std::endl;
    ofs << std::setw(3) << _antibottom.id << "  " << _antibottom.color << "  " << _antibottom.flow << "  " << antiBottom.Px() << "  " << antiBottom.Py() << "  " << antiBottom.Pz() << std::endl;
    ofs << std::setw(3) << _spectatorQuark.id << "  " << _spectatorQuark.color << "  " << _spectatorQuark.flow << "  " << quarkSpec.Px() << "  " << quarkSpec.Py() << "  " << quarkSpec.Pz() << std::endl;
        
    Size++;
    tree->Fill();
    if(i%1000==0) std::cout<<"event: "<<i<<std::endl;
  }
  hfile.Write();
  hfile.Close();
  std::cout << "Generated " << Size << " events" << std::endl;
  std::clock_t end = clock();
  double elapsed_secs = double(end-begin)/CLOCKS_PER_SEC;
  std::cout << "Generation in total took " << elapsed_secs << " seconds." << std::endl;

  //    TApplication theApp("tapp",&argc,argv);
  //
  //    TCanvas* c1 = new TCanvas("c1", "After",0.,0.,800,600);
  //    hCosTheta->SetMinimum(0.);
  //    hCosTheta->DrawNormalized("P");
  //    hCosTheta->Fit("pol2");
  //    c1->Show();
  //    TCanvas* c2 = new TCanvas("c2", "After",0.,0.,800,600);
  //    hPhi->SetMinimum(0.);
  //    hPhi->DrawNormalized("P");
  //    hPhi->Fit("pol2");
  //    c2->Show();
  //    TCanvas* c3 = new TCanvas("c3", "After",0.,0.,800,600);
  //    hCosThetaStar->SetMinimum(0.);
  //    hCosThetaStar->DrawNormalized("P");
  //    hCosThetaStar->Fit("pol2");
  //    c3->Show();
  //    TCanvas* c4 = new TCanvas("c4", "After",0.,0.,800,600);
  //    hPhiStar->SetMinimum(0.);
  //    hPhiStar->DrawNormalized("P");
  //    hPhiStar->Fit("pol2");
  //    c4->Show();
  //
  //    TCanvas* c5 = new TCanvas("c5", "Before",0.,0.,800,600);
  //    hCosThetaRe->SetMinimum(0.);
  //    hCosThetaRe->DrawNormalized("P");
  //    hCosThetaRe->Fit("pol2");
  //    c5->Show();
  //    TCanvas* c6 = new TCanvas("c6", "Before",0.,0.,800,600);
  //    hPhiRe->SetMinimum(0.);
  //    hPhiRe->DrawNormalized("P");
  //    hPhiRe->Fit("pol2");
  //    c6->Show();
  //    TCanvas* c7 = new TCanvas("c7", "Before",0.,0.,800,600);
  //    hCosThetaStarB->SetMinimum(0.);
  //    hCosThetaStarB->DrawNormalized("P");
  //    hCosThetaStarB->Fit("pol2");
  //    c7->Show();
  //    TCanvas* c8 = new TCanvas("c8", "Before",0.,0.,800,600);
  //    hPhiStarRe->SetMinimum(0.);
  //    hPhiStarRe->DrawNormalized("P");
  //    hPhiStarRe->Fit("pol2");
  //    c8->Show();
  //
  //    theApp.Run(kTRUE);
  return 0;
}

