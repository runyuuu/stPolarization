# CMakeLists.txt for event package. It creates a library with dictionary and a main program
cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
project(stPolarization)

# You need to tell CMake where to find the ROOT installation. This can be done in a number of ways:
#   - ROOT built with classic configure/make use the provided $ROOTSYS/etc/cmake/FindROOT.cmake
#   - ROOT built with CMake. Add in CMAKE_PREFIX_PATH the installation prefix for ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

#---Locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS)
find_package(ROOT REQUIRED COMPONENTS RIO Net Core MathCore Hist)

#---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
include(${ROOT_USE_FILE})
include_directories(include)

#---Create  a main program using the library
set(SOURCES src/main.cpp src/pdf.cpp src/getAngles.cpp src/construction.cpp src/generator.cpp src/AtlasStyle.C include/stPolarization.h include/protosDef.h)
set(HEADERS
   ${CMAKE_CURRENT_SOURCE_DIR}/include/stPolarization.h
   ${CMAKE_CURRENT_SOURCE_DIR}/include/protosDef.h
   ${CMAKE_CURRENT_SOURCE_DIR}/include/AtlasStyle.h

)
add_executable(stPolarization src/main.cpp)
target_link_libraries(stPolarization ${ROOT_LIBRARIES})
