//
//  main.cpp
//
//
//  Created by Runyu on 10/31/17.
//
#include "protosDef.h"
#include "construction.cpp"
#include "getAngles.cpp"
#include <iomanip>

int main(int argc, char **argv)
{
    std::clock_t begin = clock();
    double Size(0);
    //SetAtlasStyle();
    gROOT->Reset();
    std::vector<singleTop> sT;

    //std::string filename = "../../Events/sample" + pol + ".wgt";
    std::string filename = "/Users/runyu/Documents/Git/CERN/stPolarization/Events/rest.wgt";
    std::ifstream inStream(filename);
    if (inStream.is_open()) {
        while (!inStream.eof()) {
            singleTop a;
            inStream >> a;
            sT.push_back(a);
        }
    }else{
        std::cout
        << "The event file is not found!\n"
        << "Quitting..."
        << std::endl;
        return 1;
    }
    std::ofstream ofs ("sTsample.wgt", std::ofstream::out);

    TFile hfile("sampleRef.root","RECREATE","Protos Output analysis.");
    typedef struct {Float_t cos_Theta,Phi_,cos_Theta_Star,Phi_Star;} FOURANGLES;
    static FOURANGLES fourangles;
    typedef struct {Float_t cos_Theta_X,cos_Theta_Y,cos_Theta_Z;} XYZ;
    static XYZ _xyz;
    typedef struct {Float_t eventNumber, weight, eventQ;} BOOKKEEPING;
    static BOOKKEEPING _book;
    typedef struct {Float_t id,color,flow,pt,eta,phi,mass;} PARTICLE;
    static PARTICLE _top;
    static PARTICLE _W;
    static PARTICLE _bottom;
    static PARTICLE _antibottom;
    static PARTICLE _spectatorQuark;
    static PARTICLE _lepton;
    static PARTICLE _neutrino;
    typedef struct {Float_t id,color,flow,pz,mass;} BEAMLINE;
    static BEAMLINE _beam1;
    static BEAMLINE _beam2;

    TTree *tree = new TTree("singleTop","Angles of such polarization.");
    tree->Branch("fourangles",&fourangles,"cos_Theta:Phi_:cos_Theta_Star:Phi_Star");
    tree->Branch("xyz",&_xyz,"cos_Theta_X:cos_Theta_Y:cos_Theta_Z");
    tree->Branch("Event",&_book,"eventNumber:weight:eventQ");
    tree->Branch("_top",&_top,"id:color:flow:pt:eta:phi:mass");
    tree->Branch("_W",&_W,"id:color:flow:pt:eta:phi:mass");
    tree->Branch("_bottom",&_bottom,"id:color:flow:pt:eta:phi:mass");
    tree->Branch("_antibottom",&_bottom,"id:color:flow:pt:eta:phi:mass");
    tree->Branch("_spectatorQuark",&_spectatorQuark,"id:color:flow:pt:eta:phi:mass");
    tree->Branch("_beam1",&_beam1,"id:color:flow:pz:mass");
    tree->Branch("_beam2",&_beam2,"id:color:flow:pz:mass");
    tree->Branch("_lepton",&_lepton,"id:color:flow:pt:eta:phi:mass");
    tree->Branch("_neutrino",&_neutrino,"id:color:flow:pt:eta:phi:mass");


    TH1D* hCosTheta = new TH1D("hCosTheta", "CosTheta",50,-1,1);
    hCosTheta -> SetMarkerStyle(2);
    hCosTheta -> SetMinimum(0);
    TH1D* hPhi = new TH1D("hPhi", "Phi",50,0,2*M_PI);
    hPhi -> SetMarkerStyle(2);
    hPhi -> SetMinimum(0);
    TH1D* hCosThetaStar = new TH1D("hCosThetaStar", "CosThetaStar",50,-1,1);
    hCosThetaStar -> SetMarkerStyle(2);
    hCosThetaStar -> SetMinimum(0);
    TH1D* hPhiStar = new TH1D("hPhiStar", "PhiStar",50,0,2*M_PI);
    hPhiStar -> SetMarkerStyle(2);
    hPhiStar -> SetMinimum(0);
    TH1D* hCosThetaX = new TH1D("hCosThetaX", "CosThetaX",50,-1,1);
    hCosThetaX -> SetMarkerStyle(2);
    hCosThetaX -> SetMinimum(0);
    TH1D* hCosThetaY = new TH1D("hCosThetaY", "CosThetaY",50,-1,1);
    hCosThetaY -> SetMarkerStyle(2);
    hCosThetaY -> SetMinimum(0);
    TH1D* hCosThetaZ = new TH1D("hCosThetaZ", "CosThetaZ",50,-1,1);
    hCosThetaZ -> SetMarkerStyle(2);
    hCosThetaZ -> SetMinimum(0);

    
    for (int i = 0; i < sT.size(); i++) {
        TLorentzVector beam;
        TLorentzVector beamT;
        TLorentzVector top;
        TLorentzVector W;
        TLorentzVector bottom;
        TLorentzVector bottomT;
        TLorentzVector bottomW;
        TLorentzVector antiBottom;
        TLorentzVector quarkSpec;
        TLorentzVector quarkSpecT;
        TLorentzVector quarkSpecW;
        TLorentzVector lepton;
        TLorentzVector leptonT;
        TLorentzVector leptonW;
        TLorentzVector neutrino;
        TLorentzVector neutrinoT;
        TLorentzVector neutrinoW;
        TLorentzVector buffer;
        TLorentzVector WT;
        Float_t cosTheta;
        Float_t cosThetaStar;
        Float_t phi;
        Float_t phiStar;
        Float_t cosThetaX;
        Float_t cosThetaY;
        Float_t cosThetaZ;
        Float_t pW;
        Float_t pL;
        Float_t pB;
        Float_t pN;
        TVector3 T; //X-axis in top
        TVector3 N; //-Y-axis
        TVector3 q; //Z-axis
        TLorentzVector XB;
        TLorentzVector YB;

        TVector3 Tprime; //X-axis in W
        TVector3 Nprime; //-Y-axis
        TVector3 qprime; //Z-axis
        
        _book.eventNumber = sT[i].eventNumber;
        _book.weight = sT[i].weight;
        _book.eventQ = sT[i].eventQ;
        
        beam = getBeam(sT[i]);
        _beam1.id = sT[i].qg[0].particleId;
        _beam1.color = sT[i].qg[0].color;
        _beam1.flow = sT[i].qg[0].flow;
        _beam1.pz = sT[i].qg[0].pZ;
        _beam1.mass = particleMass(sT[i].qg[0].particleId);
        
        _beam2.id = sT[i].qg[1].particleId;
        _beam2.color = sT[i].qg[1].color;
        _beam2.flow = sT[i].qg[1].flow;
        _beam2.pz = sT[i].qg[1].pZ;
        _beam2.mass = particleMass(sT[i].qg[1].particleId);
        
        neutrino = getNeutrino(sT[i]);
        _neutrino.color = sT[i].bblvq[0].color;
        _neutrino.flow = sT[i].bblvq[0].flow;
        lepton = getLepton(sT[i]);
        _lepton.color = sT[i].bblvq[1].color;
        _lepton.flow = sT[i].bblvq[1].flow;
        bottom = getBottom(sT[i]);
        _bottom.color = sT[i].bblvq[2].color;
        _bottom.flow = sT[i].bblvq[2].flow;
        
        antiBottom = getAntiBottom(sT[i]);
        _antibottom.id = sT[i].bblvq[3].particleId;
        _antibottom.color = sT[i].bblvq[3].color;
        _antibottom.flow = sT[i].bblvq[3].flow;
        _antibottom.pt = antiBottom.Pt();
        _antibottom.eta = antiBottom.Eta();
        _antibottom.phi = antiBottom.Phi();
        _antibottom.mass = antiBottom.M();
        
        quarkSpec = getQuarkSpec(sT[i]);
        _spectatorQuark.id = sT[i].bblvq[4].particleId;
        _spectatorQuark.color = sT[i].bblvq[4].color;
        _spectatorQuark.flow = sT[i].bblvq[4].flow;
        _spectatorQuark.pt = quarkSpec.Pt();
        _spectatorQuark.eta = quarkSpec.Eta();
        _spectatorQuark.phi = quarkSpec.Phi();
        _spectatorQuark.mass = quarkSpec.M();
        
        //Reconstruct top and W; Boost W into top and record its momentum:
        top = neutrino + bottom + lepton;
        _top.pt = top.Pt();
        _top.eta = top.Eta();
        _top.phi = top.Phi();
        _top.mass = top.M();
        if (sT[i].bblvq[0].particleId > 0) {
            _top.id = 6;
        }else{
            _top.id = -6;
        }
        
        //Boost W, bottom spectator quark and beam into top;
        W = neutrino + lepton; // W = N + lepton ; only wants its abs momentum.
        buffer = W;
        buffer.Boost(-top.BoostVector());
        WT = buffer;
        pW = WT.P();
        buffer = bottom;
        buffer.Boost(-top.BoostVector());
        pB = buffer.P();
        bottomT = buffer;
        
        buffer = lepton;
        buffer.Boost(-top.BoostVector());
        leptonT = buffer;
        buffer = neutrino;
        buffer.Boost(-top.BoostVector());
        neutrinoT = buffer;
        
        buffer = quarkSpec;
        buffer.Boost(-top.BoostVector());
        quarkSpecT = buffer;
        buffer = beam;
        buffer.Boost(-top.BoostVector());
        beamT = buffer;
        
        //Construct q,N,T coord:
        q = quarkSpecT.Vect(); //Z-axis defined.
        q.SetMag(1);
        N = beamT.Vect().Cross(q);  //-Y-axis defined. N = st X q.
        N.SetMag(1);
        T = q.Cross(N); //X-axis defined. T = q X N.
        T.SetMag(1);
        
        XB.SetVectM(T, 0);
        YB.SetVectM(-N, 0);
        
        //Boost spectator, bottom, lepton and neutrino into W; record their momentums:
        buffer = quarkSpecT;
        buffer.Boost(-WT.BoostVector());
        quarkSpecW = buffer;
        
        buffer = bottomT;
        buffer.Boost(-WT.BoostVector());
        bottomW = buffer;
        
        buffer = leptonT;
        buffer.Boost(-WT.BoostVector());
        leptonW = buffer;
        pL = leptonW.P();
        
        buffer = neutrinoT;
        buffer.Boost(-WT.BoostVector());
        pN = buffer.P();

        //Boost again spectator, bottom, lepton and neutrino into new W:
        buffer = quarkSpecT;
        buffer.Boost(-WT.BoostVector());
        quarkSpecW = buffer;
        
        buffer = bottomT;
        buffer.Boost(-WT.BoostVector());
        bottomW = buffer;
    
        //construct q',N',T' coord:
        qprime = -bottomW.Vect(); //Z-axis defined.
        qprime.SetMag(1);
        XB.Boost(-WT.BoostVector());
        YB.Boost(-WT.BoostVector());
        Nprime = -(XB.Vect().Cross(YB.Vect())).Cross(qprime);
        //(problematic definition) Nprime = quarkSpecW.Vect().Cross(qprime);  //-Y-axis defined. N = st X q.
        Nprime.SetMag(1);
        Tprime = qprime.Cross(Nprime); //X-axis defined. T = q X N.
        Tprime.SetMag(1);
    
        //Boost everything back into lab frame. Record everything:
        buffer = WT;
        buffer.Boost(top.BoostVector());
        W = buffer;
        _W.pt = W.Pt();
        _W.eta = W.Eta();
        _W.phi = W.Phi();
        _W.mass = W.M();
        if (sT[i].bblvq[0].particleId > 0) {
            _W.id = 34;
        }else{
            _W.id = -34;
        }
        buffer = bottomT;
        buffer.Boost(top.BoostVector());
        bottom = buffer;
        _bottom.pt = bottom.Pt();
        _bottom.eta = bottom.Eta();
        _bottom.phi = bottom.Phi();
        _bottom.mass = bottom.M();
        _bottom.id = sT[i].bblvq[2].particleId;
        _bottom.color = sT[i].bblvq[2].color;
        _bottom.flow = sT[i].bblvq[2].flow;
        
        //Boost them back into lab frame. Record everything. (Going back to the top frame -> lab frame)
        buffer = leptonW;
        buffer.Boost(WT.BoostVector());
        leptonT = buffer;
        buffer.Boost(top.BoostVector());
        lepton = buffer;
        
        buffer = neutrinoW;
        buffer.Boost(WT.BoostVector());
        neutrinoT = buffer;
        buffer.Boost(top.BoostVector());
        neutrino = buffer;
        
        _lepton.id = sT[i].bblvq[1].particleId;
        _lepton.color = sT[i].bblvq[1].color;
        _lepton.flow = sT[i].bblvq[1].flow;
        _lepton.pt = lepton.Pt();
        _lepton.eta = lepton.Eta();
        _lepton.phi = lepton.Phi();
        _lepton.mass = lepton.M();
        
        _neutrino.id = sT[i].bblvq[0].particleId;
        _neutrino.color = sT[i].bblvq[0].color;
        _neutrino.flow = sT[i].bblvq[0].flow;
        _neutrino.pt = neutrino.Pt();
        _neutrino.eta= neutrino.Eta();
        _neutrino.phi = neutrino.Phi();
        _neutrino.mass = neutrino.M();

        //Get the thetaXYZ angles:
        cosThetaX = cos(leptonT.Angle(T));
        cosThetaY = cos(leptonT.Angle(-N));
        cosThetaZ = cos(leptonT.Angle(quarkSpecT.Vect()));
        _xyz.cos_Theta_X = cosThetaX;
        _xyz.cos_Theta_Y = cosThetaY;
        _xyz.cos_Theta_Z = cosThetaZ;
        hCosThetaX->Fill(cosThetaX,sT[i].weight);
        hCosThetaY->Fill(cosThetaY,sT[i].weight);
        hCosThetaZ->Fill(cosThetaZ,sT[i].weight);
        
        
        //Calculating angles again:
        
        cosTheta = cos(WT.Angle(quarkSpecT.Vect()));
        phi = getPhi(top,W,T,-N,q);// phi = angle between x axis and W's projection on x-y plane.
        cosThetaStar = cos(leptonW.Angle(-bottomW.Vect()));
        phiStar = getPhi(WT,leptonT,Tprime,-Nprime,qprime);// phi* = angle between x axis and l's projection on x-y plane (W frame).
        
        fourangles.cos_Theta = cosTheta;
        fourangles.Phi_ = phi;
        fourangles.cos_Theta_Star = cosThetaStar;
        fourangles.Phi_Star=phiStar;

        
        //Plot the generated 4 angles.
        hCosTheta->Fill(cosTheta,_book.weight);
        hPhi->Fill(phi,_book.weight);
        hCosThetaStar->Fill(cosThetaStar,_book.weight);
        hPhiStar->Fill(phiStar,_book.weight);
        
        tree->Fill();
        //Write into Protos-like txt file.
        ofs << std::setw(10) << sT[i].eventNumber << std::setw(15) << sT[i].weight << std::setw(10) <<  sT[i].eventQ << std::endl;
        ofs << std::setw(3) << std::right << sT[i].qg[0].particleId << std::setw(5) << std::right << sT[i].qg[0].color << std::setw(5) << std::right << sT[i].qg[0].flow << std::setw(10) << std::right << sT[i].qg[0].pZ << std::endl;
        ofs << std::setw(3) << sT[i].qg[1].particleId << std::setw(5) << sT[i].qg[1].color << std::setw(5) << sT[i].qg[1].flow << std::setw(10) << sT[i].qg[1].pZ << std::endl;
        
        ofs << std::setw(3) << _neutrino.id << std::setw(5) << _neutrino.color << std::setw(5) << _neutrino.flow << std::setw(10) << std::right << neutrino.Px() << std::setw(10) << std::right << std::right << neutrino.Py() << std::setw(10) << std::right <<  std::right << neutrino.Pz() << std::endl;
        ofs << std::setw(3) << _lepton.id << std::setw(5) <<_lepton.color << std::setw(5) << _lepton.flow << std::setw(10) << lepton.Px() << std::setw(10) << lepton.Py() << std::setw(10) << lepton.Pz() << std::endl;
        ofs << std::setw(3) << _bottom.id << std::setw(5) << _bottom.color << std::setw(5) << _bottom.flow << std::setw(10) << bottom.Px() << std::setw(10) << bottom.Py() << std::setw(10) << bottom.Pz() << std::endl;
        ofs << std::setw(3) << _antibottom.id << std::setw(5) << _antibottom.color << std::setw(5) << _antibottom.flow << std::setw(10) << antiBottom.Px() << std::setw(10) << antiBottom.Py() << std::setw(10) << antiBottom.Pz() << std::endl;
        ofs << std::setw(3) << _spectatorQuark.id << std::setw(5) << _spectatorQuark.color << std::setw(5) << _spectatorQuark.flow << std::setw(10) << quarkSpec.Px() << std::setw(10) << quarkSpec.Py() << std::setw(10) << quarkSpec.Pz() << std::endl;
        Size++;
    }
    hfile.Write();
    hfile.Close();
    std::clock_t end = clock();
    double elapsed_secs = double(end-begin)/CLOCKS_PER_SEC;
    std::cout << "Generation in total took " << elapsed_secs << " seconds." << std::endl;

//    TApplication theApp("tapp",&argc,argv);
//
//    TCanvas* c1 = new TCanvas("c1", "After",0.,0.,800,600);
//    hCosTheta->SetMinimum(0.);
//    hCosTheta->DrawNormalized("P");
//    hCosTheta->Fit("pol2");
//    c1->Show();
//    TCanvas* c2 = new TCanvas("c2", "After",0.,0.,800,600);
//    hPhi->SetMinimum(0.);
//    hPhi->DrawNormalized("P");
//    hPhi->Fit("pol2");
//    c2->Show();
//    TCanvas* c3 = new TCanvas("c3", "After",0.,0.,800,600);
//    hCosThetaStar->SetMinimum(0.);
//    hCosThetaStar->DrawNormalized("P");
//    hCosThetaStar->Fit("pol2");
//    c3->Show();
//    TCanvas* c4 = new TCanvas("c4", "After",0.,0.,800,600);
//    hPhiStar->SetMinimum(0.);
//    hPhiStar->DrawNormalized("P");
//    hPhiStar->Fit("pol2");
//    c4->Show();
//
//    TCanvas* c5 = new TCanvas("c5", "Before",0.,0.,800,600);
//    hCosThetaRe->SetMinimum(0.);
//    hCosThetaRe->DrawNormalized("P");
//    hCosThetaRe->Fit("pol2");
//    c5->Show();
//    TCanvas* c6 = new TCanvas("c6", "Before",0.,0.,800,600);
//    hPhiRe->SetMinimum(0.);
//    hPhiRe->DrawNormalized("P");
//    hPhiRe->Fit("pol2");
//    c6->Show();
//    TCanvas* c7 = new TCanvas("c7", "Before",0.,0.,800,600);
//    hCosThetaStarB->SetMinimum(0.);
//    hCosThetaStarB->DrawNormalized("P");
//    hCosThetaStarB->Fit("pol2");
//    c7->Show();
//    TCanvas* c8 = new TCanvas("c8", "Before",0.,0.,800,600);
//    hPhiStarRe->SetMinimum(0.);
//    hPhiStarRe->DrawNormalized("P");
//    hPhiStarRe->Fit("pol2");
//    c8->Show();
//
//    theApp.Run(kTRUE);
    return 0;
}

