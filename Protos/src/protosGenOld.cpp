#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <random>

#include <TROOT.h>
#include <TStyle.h>
#include "TLorentzVector.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TRatioPlot.h"

#include "../include/protosDef.h"
#include "getAngles.cpp"
#include "construction.cpp"
#include "TApplication.h"
#include "AtlasStyle.C"


int main(int argc, char **argv)
{
    SetAtlasStyle();
    gROOT->Reset();
    TApplication theApp("tapp", &argc, argv);
    std::ifstream inStream("../../Events/tbj.wgt");
    std::vector<singleTop> sT;
    
    while (!inStream.eof()) {
        singleTop a;
        inStream >> a;
        sT.push_back(a);
    }
    
    auto *hTheta = new TH1F("CosTheta", "CosTheta", 30, -1, 1);
    hTheta -> GetXaxis()->SetTitle("cos(#theta)");
    
    auto *hPhi = new TH1F("Phi", "Phi", 30, -M_PI, M_PI);
    hPhi -> GetXaxis()->SetTitle("#phi");
    
    auto *hThetaStar = new TH1F("CosThetaStar", "CosThetaStar", 30, -1, 1);
    hThetaStar -> GetXaxis()->SetTitle("cos(#theta *)");
    
    auto *hPhiStar = new TH1F("PhiStar", "PhiStar", 30, -M_PI, M_PI);
    hPhiStar -> GetXaxis()->SetTitle("#phi *");
    
    auto *hZ = new TH1F("CosThetaZ", "CosThetaZ", 30, -1, 1);
    //hZ -> SetMarkerStyle(2);
    hZ -> GetXaxis()->SetTitle("cos(#theta Z)");

    for (int i = 0; i < sT.size(); i++) {
        TLorentzVector beam;
        TLorentzVector beamT;
        TLorentzVector top;
        TLorentzVector W;
        TLorentzVector bottom;
        TLorentzVector bottomT;
        TLorentzVector bottomW;
        TLorentzVector antiBottom;
        TLorentzVector quarkSpec;
        TLorentzVector quarkSpecT;
        TLorentzVector quarkSpecW;
        TLorentzVector lepton;
        TLorentzVector neutrino;
        TLorentzVector buffer;
        TLorentzVector WT;
        double cosTheta;
        double cosThetaStar;
        double phi;
        double phiStar;
        double cosThetaZ;
        TVector3 T; //X-axis in top
        TVector3 N; //-Y-axis
        TVector3 q; //Z-axis
        
        TVector3 Tprime; //X-axis in W
        TVector3 Nprime; //-Y-axis
        TVector3 qprime; //Z-axis
        
        beam = getBeam(sT[i]);
        neutrino = getNeutrino(sT[i]);
        lepton = getLepton(sT[i]);
        bottom = getBottom(sT[i]);
        antiBottom = getAntiBottom(sT[i]);
        quarkSpec = getQuarkSpec(sT[i]);
        
        buffer = quarkSpec;
        top = neutrino + bottom + lepton;
        
        buffer.Boost(-top.BoostVector());
        quarkSpecT = buffer; //Get Spectator quark in top rest frame.
        buffer = beam;
        buffer.Boost(-top.BoostVector()); //Boost the lab-z into top rest frame.
        beamT = buffer;
        
        buffer = neutrino + lepton; // W = N + lepton
        W = buffer;
        buffer.Boost(-top.BoostVector());
        WT = buffer;
        
        buffer = quarkSpec;
        buffer.Boost(-top.BoostVector());
        quarkSpecT = buffer;
        buffer.Boost(-WT.BoostVector());
        quarkSpecW = buffer;
        
        buffer = bottom;
        buffer.Boost(-top.BoostVector());
        bottomT = buffer;
        buffer.Boost(-WT.BoostVector());
        bottomW = buffer;
        
        q = quarkSpecT.Vect(); //Z-axis defined.
        q.SetMag(1);
        N = beam.Vect().Cross(q);  //-Y-axis defined. N = st X q.
        N.SetMag(1);
        T = q.Cross(N); //X-axis defined. T = q X N.
        T.SetMag(1);
//        T = -q.Cross(beam.Vect());  //-Y-axis defined. N = st X q.
//        T.SetMag(1);
//        N = T.Cross(-q); //X-axis defined. T = q X N.
//        N.SetMag(1);
        
        qprime = -bottomW.Vect(); //Z-axis defined.
        qprime.SetMag(1);
        Nprime = quarkSpecW.Vect().Cross(qprime);  //-Y-axis defined. N = st X q.
        Nprime.SetMag(1);
        Tprime = qprime.Cross(Nprime); //X-axis defined. T = q X N.
        Tprime.SetMag(1);
//        Tprime = -qprime.Cross(quarkSpecW.Vect());  //-Y-axis defined. N = st X q.
//        Tprime.SetMag(1);
//        Nprime = Tprime.Cross(-qprime); //X-axis defined. T = q X N.
//        Nprime.SetMag(1);

//        buffer.SetPxPyPzE(0,0,0,1); //Setting an arbitrary lab frame vector.
//        TVector3 labX(1,0,0);
//        TVector3 labY(0,1,0);
//        TVector3 labZ(0,0,1);

        cosTheta = getCosTheta(top,quarkSpec,W);
        phi = getPhi(top,W,T,N,q);// phi = angle between x axis and W's projection on x-y plane.
    
        cosThetaStar = getCosTheta(W,lepton,-bottom); //bottom quark points at the opposite direction of W in top.
        phiStar = getPhi(W,lepton,Tprime,-Nprime,qprime);// phi* = angle between x axis and l's projection on x-y plane (W frame).
        cosThetaZ = getCosTheta(top,quarkSpec,lepton);
        
        hTheta->Fill(cosTheta,sT[i].weight);
        hPhi->Fill(phi,sT[i].weight);
        hThetaStar->Fill(cosThetaStar,sT[i].weight);
        hPhiStar->Fill(phiStar,sT[i].weight);
        hZ->Fill(cosThetaZ,sT[i].weight);
    }
    
    TCanvas* cTheta = new TCanvas("cTheta","Cos(Theta)",0.,0.,800,600);
    hTheta->Scale(1/hTheta->Integral());
    hTheta->Draw("P");
    hTheta->Fit("pol2");
    cTheta->Clear();
    auto rpTheta = new TRatioPlot(hTheta,"P");
    rpTheta->SetGraphDrawOpt("P");
    rpTheta->Draw();
    rpTheta->GetLowerRefYaxis()->SetTitle("ratio");
    rpTheta->SetUpBottomMargin(0);
    cTheta->Update();
    
    TCanvas* cPhi = new TCanvas("cPhi","Phi",0.,0.,800,600);
    hPhi->Scale(1/hPhi->Integral());
    hPhi->SetMinimum(0);
    hPhi->Draw("P");
    hPhi->Fit("pol9");
    cPhi->Clear();
    auto rpPhi = new TRatioPlot(hPhi);
    rpPhi->SetGraphDrawOpt("P");
    rpPhi->Draw();
    rpPhi->GetLowerRefYaxis()->SetTitle("ratio");
    cPhi->Update();
    
    TCanvas* cThetaStar = new TCanvas("cThetaStar","Cos(Theta*)",0.,0.,800,600);
    hThetaStar->Scale(1/hThetaStar->Integral());
    hThetaStar->SetMinimum(0);
    hThetaStar ->Draw("P");
    hThetaStar->Fit("pol2");
    cThetaStar->Clear();
    auto rpThetaStar = new TRatioPlot(hThetaStar);
    rpThetaStar->SetGraphDrawOpt("P");
    rpThetaStar->Draw();
    rpThetaStar->GetLowerRefYaxis()->SetTitle("ratio");
    cThetaStar->Update();
    
    TCanvas* cPhiStar = new TCanvas("cPhiStar","Phi*",0.,0.,800,600);
    hPhiStar->Scale(1/hPhiStar->Integral());
    hPhiStar->SetMinimum(0);
    hPhiStar->Draw("P");
    hPhiStar->Fit("pol9");
    cPhiStar->Clear();
    auto rpPhiStar = new TRatioPlot(hPhiStar);
    rpPhiStar->SetGraphDrawOpt("P");
    rpPhiStar->Draw();
    rpPhiStar->GetLowerRefYaxis()->SetTitle("ratio");
    //rpPhiStar->GetUpperRefYaxis()->SetTitle("Entry Ratio");
    cPhiStar->Update();
    
    TCanvas* cZ = new TCanvas("cZ","ThetaZ",0.,0.,800,600);
    //auto *c5 = new TCanvas("c5","ThetaX");
    hZ->Scale(1/hZ->Integral());
    hZ->SetMinimum(0);
    hZ->Draw("P");
    hZ->Fit("pol2");
    cZ -> Clear();
    auto rpZ = new TRatioPlot(hZ);
    rpZ->SetGraphDrawOpt("P");
    rpZ->SetUpBottomMargin(10);
    rpZ->Draw();
    rpZ->GetLowerRefYaxis()->SetTitle("ratio");
    rpZ->GetUpperRefYaxis()->SetTitle("Entry Ratio");
    cZ->Update();

    theApp.Run(kTRUE);
    theApp.Terminate(0);
    return 0;
}


